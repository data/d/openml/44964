# OpenML dataset: superconductivity

https://www.openml.org/d/44964

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Data Description**

Dataset contains data on 21263 superconductors and their relevant features including the critical temperature. The goal here is to predict the latter based on the features extracted.

**Attribute Description**

All features describe the features of superconductors with the *critical_temp* as target feature.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/44964) of an [OpenML dataset](https://www.openml.org/d/44964). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/44964/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/44964/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/44964/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

